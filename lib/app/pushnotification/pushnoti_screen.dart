import 'package:flutter/material.dart';

import '../../consts.dart';

class PushNotification extends StatefulWidget {
  @override
  _PushNotificationState createState() => _PushNotificationState();
}

class _PushNotificationState extends State<PushNotification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Thông báo", style: TextStyle(color: Colors.white)),
        automaticallyImplyLeading: false,
        backgroundColor: Consts.backgroundApp,
      ),
      body: Container(),
    );
  }
}
