import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../consts.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String idCollecttionDay;
  String userName = Consts.userName;
  String location = Consts.job;
  DateTime dateToday =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  final db = FirebaseFirestore.instance;
  TextEditingController controller = new TextEditingController();
  String mess = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Consts.backgroundApp,
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_sharp),
        ),
        title: Text(
          'Thông Tin Cá Nhân',
          overflow: TextOverflow.ellipsis,
        ),
        actions: [
          IconButton(
            onPressed: () {
              _showMyDialog();
            },
            icon: Icon(Icons.edit_outlined),
          ),
        ],
        automaticallyImplyLeading: false,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          cover(),
          userInfor(),
          Divider(thickness: 0.5),
          Padding(
            padding: EdgeInsets.only(left: 12, right: 12),
            child: Row(
              children: [
                Container(
                  width: 110,
                  child: Text('Date'),
                ),
                Spacer(flex: 2),
                Text('Check in'),
                Spacer(),
                Text('Check out'),
              ],
            ),
          ),
          Divider(thickness: 2),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance
                  .collection('ChamCong')
                  .doc(Consts.document)
                  .collection(Consts.month)
                  .orderBy('time')
                  .snapshots(),
              builder: (context, snapshots) {
                if (snapshots.hasData) {
                  return ListView.builder(
                    shrinkWrap: true,
                    itemCount: snapshots.data.docs.length,
                    itemBuilder: (context, index) {
                      DocumentSnapshot ds = snapshots.data.docs[index];
                      return Padding(
                          padding: EdgeInsets.only(left: 12, right: 12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Container(
                                      width: 100,
                                      child: Text(ds["check_in_day"])),
                                  Spacer(flex: 2),
                                  Text(ds["check_in_time"]),
                                  Spacer(),
                                  ds["check_out"] == null
                                      ? SizedBox(width: 50)
                                      : Text(ds["check_out"]),
                                ],
                              ),
                              Divider()
                            ],
                          ));
                    },
                  );
                }
                return Container();
              },
            ),
          )
        ],
      ),
    );
  }

  Widget cover() {
    return Container(
      height: 225,
      child: Stack(
        children: [
          Container(
            height: 200,
            color: Colors.grey[350],
            child: Image(
              image: NetworkImage(Consts.cover),
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            // child: Image(
            //   image: AssetImage('images/test_cover.jpg'),
            //   width: MediaQuery.of(context).size.width,
            //   fit: BoxFit.cover,
            // ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: avatar(),
          ),

        ],
      ),
    );
  }

  Widget avatar() {
    return Container(
      height: 130,
      width: 110,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20),
            height: 110,
            width: 110,
            decoration: BoxDecoration(
                border: Border.all(width: 1.5, color: Colors.white),
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: NetworkImage(Consts.avatar),
                    fit: BoxFit.cover)),
          ),
        ],
      ),
    );
  }

  Widget userInfor() {
    return Center(
      child: Column(
        children: [
          SizedBox(height: 10),
          Container(
            width: 300,
            height: 30,
            child: Center(
              child: Text(
                userName,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: 30,
            child: Center(
              child: Text(
                location,
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 12),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Thông Báo'),
          content: SizedBox(
            height: 70,
            child: Center(
              child: Text(
                'Hiện Tại Bạn Chưa Sử Dụng Được Tính Năng Này, Chúng Tôi Sẽ Hoàn Thiện Trong Thời Gian Tới. Xin Cảm ơn !!!',
              ),
            ),
            width: MediaQuery.of(context).size.width - 10,
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Đóng'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }
}
