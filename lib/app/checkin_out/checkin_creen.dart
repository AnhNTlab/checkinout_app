import 'dart:async';
import 'package:cham_cong_app/Routes.dart';
import 'package:cham_cong_app/app/profile/profile_creen.dart';
import 'package:cham_cong_app/app/scanQR/scan_qr_code.dart';
import 'package:cham_cong_app/app/total/total.dart';
import 'package:cham_cong_app/instance%20/DataSingleton.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../consts.dart';

class ChechInOut extends StatefulWidget {
  final bool checkout;

  const ChechInOut({
    Key key,
    this.checkout,
  }) : super(key: key);

  @override
  _ChechInOutState createState() => _ChechInOutState(checkout);
}

class _ChechInOutState extends State<ChechInOut> {
  dynamic checkInLocation = "";
  dynamic checkOutLocation = "";
  double workedTime = 0.0;
  String userName = Consts.userName;
  // bool pressedCheckin = false;
  final db = FirebaseFirestore.instance;

  String idCollecttionDay;
  //String idOnline;

  bool checkout;
  _ChechInOutState(this.checkout);
  int check = 1;
  bool startispressed = true;
  bool stoptispressed = true;
  String timedisplay = "00:00:00";
  String savetime;
  var swatch = Stopwatch();
  final duration = const Duration(seconds: 1);

  void starttimer() {
    Timer(duration, callbackfuntion);
  }

  void callbackfuntion() {
    if (swatch.isRunning) {
      starttimer();
    }
    setState(() {
      timedisplay = swatch.elapsed.inHours.toString().padLeft(2, "0") +
          ":" +
          (swatch.elapsed.inMinutes % 60).toString().padLeft(2, "0") +
          ":" +
          (swatch.elapsed.inSeconds % 60).toString().padLeft(2, "0");
    });
  }

  void startstopwatch() async {
    dynamic currentTime = DateFormat.jm().format(DateTime.now());
    var currentHour = DateTime.now().hour;
    var prefHour = currentHour*60 + DateTime.now().minute;
    DataSingleton.instance.workTime= prefHour;
    await _showMyDialog2(currentTime);
    swatch.start();
    starttimer();
    setState(() {
      stoptispressed = false;
      startispressed = false;
      DataSingleton.instance.pressedCheckIn = true;
    });
    final possitonin = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    checkInLocation = possitonin;
    var a = db
        .collection('ChamCong')
        .doc(Consts.document)
        .collection(Consts.month);
    var c;
    if(currentHour<=13){
      c = db.collection('Admin')
          .doc(((DateTime.now().weekday)+1).toString())
          .collection('online');
    } else{
      c = db.collection('Admin')
          .doc(((DateTime.now().weekday)+1).toString())
          .collection('onlineChieu');
    }

    //DateTime dateToday = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day) ;
    DateTime now = DateTime.now();
    var checkInTime = DateFormat('HH:mm:ss').format(now);
    String dateToday = DateFormat('dd-MM-yyyy').format(now);
    final save = await a.add({
      'lat_In': possitonin.latitude,
      'long_In': possitonin.longitude,
      'lat_Out': null,
      'long_Out': null,
      'sender': Consts.userName,
      'check_in_day': dateToday.toString(),
      'check_in_time': currentTime,
      'check_out': null,
      'worked_time': workedTime,
      'time': DateTime.now().millisecondsSinceEpoch
    });

    final online = await c.add({
      'sender': Consts.userName,
      'is_online': true,
      'check_in_time':checkInTime,
    });

    idCollecttionDay = save.id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('idCollectionDay', save.id);

    await prefs.setString('idOnline', online.id);

    await prefs.setInt('checkInTime', prefHour);
    // idOnline = online.id;
  }

  Future<void> stopstopwatch() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String collectionDay = prefs.getString('idCollectionDay');
    final possitonout = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    checkOutLocation = possitonout;
    dynamic currentTime = DateFormat.jm().format(DateTime.now());
    var currentHour = DateTime.now().hour;
    await db.collection('ChamCong')
        .doc(Consts.document)
        .collection(Consts.month)
        .doc(collectionDay)
        .update({
      'check_out': currentTime,
      'lat_Out': possitonout.latitude,
      'long_Out': possitonout.longitude,
      'worked_time': num.parse((((((DateTime.now().hour*60 + DateTime.now().minute)-DataSingleton.instance.workTime)))/60).toStringAsFixed(2))
      //'worked_time':  num.parse(workedTime.toStringAsFixed(2)),
    });
    if(currentHour<13){
      await db.collection('Admin')
          .doc(((DateTime.now().weekday)+1).toString())
          .collection('online')
          .doc(prefs.getString('idOnline'))
          .delete().then((value) => print('deleted complete'));
    } else{
      await db.collection('Admin')
          .doc(((DateTime.now().weekday)+1).toString())
          .collection('onlineChieu')
          .doc(prefs.getString('idOnline'))
          .delete().then((value) => print('deleted complete'));
    }

    _showMyDialog(context, currentTime, (((DateTime.now().hour*60 + DateTime.now().minute)-DataSingleton.instance.workTime)).toString());
    swatch.stop();
    if (checkout == true) {
      stoptispressed = true;
      savetime = timedisplay;
      swatch.reset();
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          actions: [
            IconButton(
              icon: Icon(Icons.message),
              onPressed: null,
            )
          ],
        ),
        bottomNavigationBar: BottomAppBar(
                elevation: 0,
                child: Padding(
                  padding: EdgeInsets.only(left: 12, right: 12, bottom: 20),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Profile()),
                          );
                        },
                        child: Container(
                            height: 30,
                            width: 90,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(8)),
                            child: Center(child: Text('Lịch sử'))),
                      ),
                      Spacer(),
                      InkWell(
                        onTap: () {},
                        child: Container(
                            height: 30,
                            width: 90,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(8)),
                            child: Center(child: Text('About'))),
                      ),
                    ],
                  ),
                ),
              ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text(
                      'Welcome',
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      userName,
                      style:
                          TextStyle(fontWeight: FontWeight.w900, fontSize: 30),
                    ),
                  ],
                ),
                SizedBox(height: 40),
                TimeOfDay(),
                SizedBox(height: 30),
                DataSingleton.instance.check==true || DataSingleton.instance.pressedCheckIn
                    ? Container(
                        width: MediaQuery.of(context).size.width - 100,
                        height: 250,
                        decoration: BoxDecoration(
                          color: Colors.green[800],
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 20, bottom: 20),
                          child: Column(
                            children: [
                              Text(
                                'Working time \ncủa bạn là:',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 30, color: Colors.white),
                              ),
                              SizedBox(height: 20),
                              Container(
                                child: Text(
                                  '${(((DateTime.now().hour*60 + DateTime.now().minute)-DataSingleton.instance.workTime))}m',
                             //     (((DateTime.now().hour*60 + DateTime.now().minute)-DataSingleton.instance.workTime)/60).toStringAsFixed(2),
                                  // timedisplay,
                                  style: TextStyle(
                                      fontSize: 50,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ))
                    : Center(
                        child: InkWell(
                          onTap: startispressed ? startstopwatch : null,
                          child: Container(
                            height: 250,
                            width: 250,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(200)),
                                border: Border.all(color: Colors.grey[700])),
                            child: Center(
                                child: Text(
                              "CHECK IN",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                          ),
                        ),
                      ),
                SizedBox(height: 50),
                DataSingleton.instance.check==true || DataSingleton.instance.pressedCheckIn
                    ? Center(
                        child: Column(
                        children: [
                          Text(checkInLocation.toString()),
                          SizedBox(height: 30),
                          InkWell(
                            // onTap: stoptispressed ? null : stopstopwatch,
                            onTap: () {
                              dynamic currentTime =
                                  DateFormat.jm().format(DateTime.now());
                              workedTime = num.parse((((DateTime.now().hour*60 + DateTime.now().minute)-DataSingleton.instance.workTime)/60).toStringAsFixed(2));
                              _showMyDialog(context, currentTime, (((DateTime.now().hour*60 + DateTime.now().minute)-DataSingleton.instance.workTime)).toString());
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width - 100,
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(10)),
                              margin: EdgeInsets.symmetric(horizontal: 30),
                              height: 80,
                              child: Center(
                                  child: Text(
                                "CHECK OUT",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              )),
                            ),
                          ),
                        ],
                      ))
                    : SizedBox(),
              ],
            ),
          ),
        ));
  }

  Future<void> _showMyDialog(context, dynamic time, String workedTime) {
    return showGeneralDialog(
      context: context,
      transitionDuration: Duration(milliseconds: 50),
      pageBuilder: (_, __, ___) {
        return Material(
          child: Container(
            color: Colors.white,
            child: Center(
              child: Container(
                height: 400,
                width: MediaQuery.of(context).size.width - 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.blue[900],
                ),
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(height: 30),
                      Text(
                        'Giờ check out của bạn là:',
                        overflow: TextOverflow.clip,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      SizedBox(height: 10),
                      Text(
                        time,
                        style: TextStyle(
                            fontSize: 50,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      SizedBox(height: 10),
                      Divider(
                        color: Colors.white,
                      ),
                      Text(
                        'Tổng thời gian làm việc của bạn là: ',
                        overflow: TextOverflow.clip,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      SizedBox(height: 20),
                      Text(
                        '${workedTime}m',
                        style: TextStyle(
                            fontSize: 50,
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      ),
                      Spacer(),
                      Text(
                        'Bạn đã chắc chắn chưa?',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding:
                            EdgeInsets.only(left: 20, right: 20, bottom: 20),
                        child: Row(
                          children: [
                            RaisedButton(
                              color: Colors.grey,
                              onPressed: () async {
                                await stopstopwatch();
                                Routes.sailor.navigate(Consts.qrEx2);
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                await prefs.setBool('boolValueCheck', false);
                                await prefs.setInt('checkInTime', 0);
                                print('${prefs.getBool('boolValueCheck')} fffffffffffffffffffffff');
                              },
                              child: Text(
                                "YES",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            Spacer(),
                            RaisedButton(
                              color: Colors.grey,
                              onPressed: () {
                                swatch.start();
                                starttimer();
                                Navigator.pop(context);
                              },
                              child: Text(
                                "NO",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  //checkin dialog
  Future<void> _showMyDialog2(dynamic time) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.blue[900],
          title: Center(
              child: Text(
            "Giờ checkin của bạn là:",
            overflow: TextOverflow.visible,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white),
          )),
          content: Container(
            height: 150,
            width: 150,
            child: Center(
              child: Column(
                children: [
                  Text(
                    time,
                    style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Bạn đã chắc chắn chưa?',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 10),
                  RaisedButton(
                    color: Colors.grey,
                    onPressed: () async {
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      await prefs.setBool('boolValueCheck', true);
                      print('${prefs.getBool('boolValueCheck')} kkkkkkkkkkkkkkkkkkkkkkkkkk');
                      Navigator.pop(context);
                    },
                    child: Text(
                      "XÁC NHẬN",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

///real time clock
class TimeOfDay extends StatefulWidget {
  @override
  _TimeOfDayState createState() => _TimeOfDayState();
}

class _TimeOfDayState extends State<TimeOfDay> {
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  String realTime = DateFormat.jms().format(DateTime.now());
  // dynamic currentTime = DateFormat.jm().format(DateTime.now());
  @override
  void initState() {
    super.initState();
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (realTime != DateFormat.jms().format(DateTime.now())) {
        setState(() {
          realTime = DateFormat.jms().format(DateTime.now());
        });
      }
    });
  }

  Widget build(BuildContext context) {
    return Text(
      realTime,
      style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
    );
  }
}

class QRViewExample2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _QRViewExample2State();
}

class _QRViewExample2State extends State<QRViewExample2> {
  String qrText = '';
  var flashState = flashOn;
  var cameraState = frontCamera;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  final db = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 9,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                borderColor: Colors.red,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: 300,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8),
                        child: RaisedButton(
                          onPressed: () {
                            if (controller != null) {
                              controller.toggleFlash();
                              if (_isFlashOn(flashState)) {
                                setState(() {
                                  flashState = flashOff;
                                });
                              } else {
                                setState(() {
                                  flashState = flashOn;
                                });
                              }
                            }
                          },
                          child:
                              Text(flashState, style: TextStyle(fontSize: 20)),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(8),
                        child: RaisedButton(
                          onPressed: () {
                            if (controller != null) {
                              controller.flipCamera();
                              if (_isBackCamera(cameraState)) {
                                setState(() {
                                  cameraState = frontCamera;
                                });
                              } else {
                                setState(() {
                                  cameraState = backCamera;
                                });
                              }
                            }
                          },
                          child:
                              Text(cameraState, style: TextStyle(fontSize: 20)),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  bool _isFlashOn(String current) {
    return flashOn == current;
  }

  bool _isBackCamera(String current) {
    return backCamera == current;
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        if (scanData ==
            "VUONGSOFT COMPANY LIMITED.\nĐịa chỉ: Tầng 7 Tòa nhà Mac Plaza, Số 10 Trần Phú, Phường Mộ Lao, Quận Hà Đông, Thành phố Hà Nội.\nMã số thuế: 0109320368.\nNgười ĐDPL: Nguyễn Đức Hải.") {
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(builder: (context) => Total()),
          // );
          Routes.sailor.navigate(Consts.totalPage);
        } else if (scanData ==
            "VUONGSOFT COMPANY LIMITED.\nĐịa chỉ: Tầng 7 Tòa nhà Mac Plaza, Số 10 Trần Phú, Phường Mộ Lao, Quận Hà Đông, Thành phố Hà Nội.\nMã số thuế: 0109320368.\nNgười ĐDPL: Nguyễn Đức Hải.") {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Profile()),
          );
        }
      });
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
/////