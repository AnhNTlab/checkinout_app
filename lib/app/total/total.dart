import 'package:cham_cong_app/Routes.dart';
import 'package:cham_cong_app/app/scanQR/scan_qr_code.dart';
import 'package:cham_cong_app/initial.dart';
import 'package:cham_cong_app/instance%20/DataSingleton.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../consts.dart';

class Total extends StatefulWidget {
  @override
  _TotalState createState() => _TotalState();
}

class _TotalState extends State<Total> {
  String a = "";
  String b = "";
  String c = "";
  @override
  void initState() {
    super.initState();
    getCheckInTime();
    getCheckOutTime();
    getWorkedTime();
  }

  Future<void> getCheckInTime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    FirebaseFirestore.instance
        .collection('ChamCong')
        .doc(Consts.document)
        .collection(Consts.month)
        .doc(prefs.getString('idCollectionDay'))
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        setState(() {
          a= documentSnapshot.data()['check_in_time'].toString();
        });
        //a= documentSnapshot.data()['sender'];
      } else {
        print('Document does not exist on the database');
      }
  });}

  Future<void> getCheckOutTime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    FirebaseFirestore.instance
        .collection('ChamCong')
        .doc(Consts.document)
        .collection(Consts.month)
        .doc(prefs.getString('idCollectionDay'))
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        setState(() {
          b= documentSnapshot.data()['check_out'];
        });
    //    b= documentSnapshot.data()['check_out'];
      } else {
        print('Document does not exist on the database');
      }
    });}

  Future<void> getWorkedTime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    FirebaseFirestore.instance
        .collection('ChamCong')
        .doc(Consts.document)
        .collection(Consts.month)
        .doc(prefs.getString('idCollectionDay'))
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        setState(() {
          c= documentSnapshot.data()['worked_time'].toString();
        });
      } else {
        print('Document does not exist on the database');
      }
    });}

  var now = DateTime.now();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Container(
            height: 550,
            width: MediaQuery.of(context).size.width - 100,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.blue[900],
            ),
            child: Center(
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Text(
                    'Ngày ${now.day}/${now.month}/${now.year}',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(height: 30),
                  Text(
                    'Giờ check in của bạn là:',
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  Text(
                    a,
                    style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(height: 30),
                  Text(
                    'Giờ check out của bạn là:',
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(height: 10),
                  Text(
                    b,
                    style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(height: 10),
                  Divider(
                    color: Colors.white,
                  ),
                  Text(
                    'Tổng thời gian làm việc của bạn là: ',
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(height: 20),
                  Text(
                    '${c}h',
                    style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                  Spacer(),
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                    child: Row(
                      children: [
                        RaisedButton(
                          color: Colors.grey,
                          onPressed: () async {
                            DataSingleton.instance.pressedCheckIn=false;
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            await prefs.setBool('boolValueCheck', false);
                            print('${prefs.getBool('boolValueCheck')} fffffffffffffffffffffff');
                            DataSingleton.instance.check = prefs.getBool('boolValueCheck');
                            Routes.sailor.navigate(Consts.initialPage);
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(
                            //       builder: (context) => InitialPage()),
                            // );
                          },
                          child: Text(
                            "New check in",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        Spacer(),
                        RaisedButton(
                          color: Colors.grey,
                          onPressed: ()  {
                              SystemChannels.platform
                                  .invokeMethod('SystemNavigator.pop');
                          },
                          child: Text(
                            "Quit",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
//////