import 'package:cham_cong_app/app/checkin_out/checkin_creen.dart';
import 'package:cham_cong_app/app/total/total.dart';
import 'package:cham_cong_app/consts.dart';
import 'package:cham_cong_app/initial.dart';
import 'package:sailor/sailor.dart';

import 'app/scanQR/scan_qr_code.dart';
class Routes {
  static final sailor = Sailor();

  static void createRoutes() {
    sailor.addRoutes(
      [
        SailorRoute(
          name: Consts.checkInScreen,
          builder: (context, args, params) {
            return ChechInOut();
          },
        ),

        SailorRoute(
          name: Consts.qrEx2,
          builder: (context, args, params) {
            return QRViewExample2();
          },
        ),

        SailorRoute(
          name: Consts.initialPage,
          builder: (context, args, params) {
            return InitialPage();
          },
        ),

        SailorRoute(
          name: Consts.totalPage,
          builder: (context, args, params) {
            return Total();
          },
        ),

        SailorRoute(
          name: Consts.qr1,
          builder: (context, args, params) => QRViewExample(),
        ),
      ],
    );
  }
}
