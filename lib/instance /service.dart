import 'package:cloud_firestore/cloud_firestore.dart';

class CloudService {
  final FirebaseFirestore _firestoreInstance = FirebaseFirestore.instance;

  Future<String> getUserNameByUID(String uid) async {
    try {
      var snapshot =
          await _firestoreInstance.collection("users").doc(uid).get();
      print(snapshot.get('name'));
      return snapshot.get('name');
    } on FirebaseException catch (e) {
      return e.code;
    }
  }
}
