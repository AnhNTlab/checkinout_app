import 'package:flutter/material.dart';

abstract class Consts {
  static const String initial = '/initial';
  static const String home = '/';
  static const String QRView = '/QRView';

  /// BackGround Color for AppBar
  static const Color backgroundAppBarColor = Color(0xFFFFFFFF);

  static const Color backgroundApp = Color(0xFF008FB4);
  static const Color backgroundGetDone = Color(0xFF56597A);

  /// Text_Title color for AppBar
  static const Color textAppBarColor = Color(0x00000000);

  static const flashOn = 'FLASH ON';
  static const flashOff = 'FLASH OFF';
  static const frontCamera = 'FRONT CAMERA';
  static const backCamera = 'BACK CAMERA';

  static const String checkInScreen = 'checkin';
  static const String qrEx2 = 'qrEx2';
  static const String initialPage = 'intialPage';
  static const String totalPage = 'totalPage';
  static const String qr1 = 'qr1';

  static const String Tam = 'XFPW7uuhYuUt8THQerIE';
  static const String TienAnh = 'b1YL6UgLpZJPqYmgBH5s';
  static const String My = 'dkQfvTYb1SCN6YVJrZJk';
  static const String Thao = 'fVSE5GOBopGhQA0E8qNc';
  static const String Thanh='Gvts1hqRpR7NNN6DIdTD';
  static const String Boss='boss';


  static const userName = 'Thu Thao';
  static const document = Thao;
  static const month = 'december';
  static const job = '~~~~~~~~~' ;
  static const cover = 'https://scontent.fhan2-6.fna.fbcdn.net/v/t1.0-9/121486154_106248984597713_1985886108350078577_o.jpg?_nc_cat=103&ccb=2&_nc_sid=e3f864&_nc_ohc=e62g-K9qw0kAX_dtgDi&_nc_ht=scontent.fhan2-6.fna&oh=2c491f42400e9da1b778b348ea4452f7&oe=5FF77124';
  static const avatar =  'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/121599755_106248314597780_2443497840324497861_n.jpg?_nc_cat=111&ccb=2&_nc_sid=09cbfe&_nc_ohc=f8GOv1OL4EYAX9eJe2v&_nc_ht=scontent.fhan2-2.fna&oh=2fc4c2ab171d5b8c1354954dfad635d5&oe=5FF485B4';

}
