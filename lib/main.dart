import 'package:cham_cong_app/Routes.dart';
import 'package:cham_cong_app/initial.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() async {
  Routes.createRoutes();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: InitialPage(),
      onGenerateRoute: Routes.sailor.generator(),
      navigatorKey: Routes.sailor.navigatorKey,
      // navigatorObservers: [Observers.routeObserver],
      // initialRoute: Consts.initial,
      // routes: {
      //   Consts.initial: (context) => InitialPage(),
      //   Consts.home: (context) => ChechInOut(checkout: false),
      //   Consts.QRView: (context) => QRViewExample(),
      // },
    );
  }
}
