import 'package:cham_cong_app/Routes.dart';
import 'package:cham_cong_app/app/profile/profile_creen.dart';
import 'package:cham_cong_app/instance%20/DataSingleton.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/checkin_out/checkin_creen.dart';
import 'app/scanQR/scan_qr_code.dart';
import 'consts.dart';

class InitialPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return StatefulWrapper(
      onInit: () {
        _init().then((value) async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            //Return bool
            DataSingleton.instance.check = prefs.getBool('boolValueCheck');
            print(DataSingleton.instance.check);
            DataSingleton.instance.idCollectionDay = prefs.getString('idCollectionDay');

            DataSingleton.instance.idOnline = prefs.getString('idOnline');

            DataSingleton.instance.workTime = prefs.getInt('checkInTime');

          DataSingleton.instance.check==false || DataSingleton.instance.check==null?
              Routes.sailor.navigate(Consts.qr1):
              Routes.sailor.navigate(Consts.checkInScreen);
            // Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //     builder: (context) => QRViewExample(),
            //   ),
            // ):
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => ChechInOut(),
          //   ),
          // );
        });
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Center(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage('images/logo.png'),
                    width: 200,
                  ),
                  Text(
                    'Sổ Chấm Công',
                    style: TextStyle(
                        color: Consts.backgroundApp,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  Future _init() async {
    await Future.delayed(Duration(seconds: 0));
  }
}

class StatefulWrapper extends StatefulWidget {
  final Function onInit;
  final Widget child;

  const StatefulWrapper({@required this.onInit, @required this.child});

  @override
  _StatefulWrapperState createState() => _StatefulWrapperState();
}

class _StatefulWrapperState extends State<StatefulWrapper> {
  @override
  void initState() {
    if (widget.onInit != null) {
      widget.onInit();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
